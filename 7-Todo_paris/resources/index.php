<?php
error_reporting(0);

include('connect.php');
include('model.php');

$list_items = Model:: factory('ListDetails')->find_many();

if (!empty($_POST)) {
  
  date_default_timezone_set("Asia/Karachi");
  $current_date = date("Y-m-d H:i:s");

  $max_position = Model:: factory('ListDetails')-> max('ItemPosition');
  $position = (int) $max_position;
  $new_position = $position+1;

  $list_value = Model:: factory('ListDetails')-> create();

  $list_value->set(array('Description' => $_POST['new-list-item-text'], 'IsDone' => 0 , 'ItemPosition' => $new_position , 'ListColor' => 0 , 'CreateDt' => $current_date));

  $list_value->save();

  header('Location: ' . basename(__FILE__));

  exit;

}


?>


<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <link rel="stylesheet" href="style.css" type="text/css" />
  <script type="text/javascript" src="Scripts/jquery-3.2.1.js"></script>
  <script type="text/javascript" src="Scripts/script.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


  <title></title>
</head>

<body>
  <div id="page-wrap">
    <div id="header">
      <h1><a href="">PHP Sample Test App</a></h1>
    </div>

    <div id="main">
      <noscript>This site just doesn't work, period, without JavaScript</noscript>

      <ul id="list" class="ui-sortable">

       
          <?php  foreach ($list_items as $list_details) {

          	$list_id = $list_details->ListItemId;

            $list_description = $list_details->Description;

            $done = $list_details->IsDone;

            $color = $list_details->ListColor;
           
            ?>
          	<li color='1' class='colorBlue ui-state-default <?php if ($done == 1): ?>line<?php endif; ?>' rel='6' data-id='<?php echo $list_id ?>'>
                   
		          	<span title='Double-click to edit...' style="background-color: <?php if($color) echo $color;?> " class="click"><?php echo $list_id.'-'.$list_description; ?></span>

		            <div class='draggertab tab'></div>

    				    <div class='colortab tab'><input class="colorpicker" type="color" style="border:none;opacity: 0;"></div>

    				    <div class='deletetab tab'  title='Double-click to delete.'></div>

    					  <div class='donetab tab'></div>	

			      </li>
       
            <?php

      }
          ?>

     
      </ul>
	  <br />

      <form action="" id="add-new" method="POST">
      
        <input type="text" id="new-list-item-text" name="new-list-item-text" class="insert" required/>
        <input type="submit" id="add-new-submit" value="Add" class="button"  />
      
      </form>

      <br>


      <div class="clear"></div>
    </div>
  </div>
</body>
</html>

