<?php

//error_reporting(0);
include('connect.php');
include('model.php');


if (isset($_POST['list_order'])) {
	try{
		$order = $_POST['list_order'];

        foreach ($order as $list_order => $list_item_id) {

        	$update_list = Model:: factory('ListDetails')->use_id_column('ListItemId')->find_one($list_item_id);

        	$update_list->set('ItemPosition', $list_order); 
            
            $update_list->save();
            
        }
	}
	catch (Exception $e){
      	echo 'Message: ' .$e->getMessage();
    }
}

if (isset($_POST['color_id'])) {

		$list_id= $_POST['color_id'];
	
		$list_color=$_POST['new_list_color'];

		$update_list = Model:: factory('ListDetails')->use_id_column('ListItemId')->find_one($list_id);
		
		$update_list->set('ListColor', $list_color);

		$update_list->save();

}


if (isset($_POST['done_id'])) {
	
		$list_id= $_POST['done_id'];

		$update_list = Model:: factory('ListDetails')->use_id_column('ListItemId')->find_one($list_id);
		
		$update_list->set('IsDone', 1); 

		$update_list->save();

}

if (isset($_POST['new_value_id'])) {
	
		$list_id= $_POST['new_value_id'];

		$list_new_value= $_POST['new_list_value'];

		$update_list = Model:: factory('ListDetails')->use_id_column('ListItemId')->find_one($list_id);
		
		$update_list->set('Description', $list_new_value);

		$update_list->save();

}


?>