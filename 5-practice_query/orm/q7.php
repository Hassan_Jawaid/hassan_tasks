<?php

include("connect.php");


$get_department = ORM::for_table('employees')
                 ->table_alias('emp')
                 ->select_many('emp.employee_id', 'emp.first_name', 'emp.last_name','emp.salary')
                 ->join('departments', array('departments.department_id', '=', 'emp.department_id'))
                 ->where_raw('(departments.department_name = "IT") AND (salary > (SELECT AVG(salary) FROM employees))')
                 ->find_many();


$count = count($get_department);

?>

<html>
    <head>
        <title>Practice Query</title>
    </head>

    <body>
    
        <?php echo $count; ?>
        <form method="post" action="">
        <table width='auto' border='1'>
            <tr>
                <th>Employee ID</th> 
                <th>First Name</th>
                <th>Last Name</th>
                <th>Salary</th>                
            </tr>
            <?php foreach ($get_department as $user_info): ?>
                <tr>
                   <td><?php echo $user_info->employee_id; ?></strong></td>&nbsp;
                   <td><?php echo $user_info->first_name; ?></strong></td>
                   <td><?php echo $user_info->last_name; ?></strong></td>
                   <td><?php echo $user_info->salary; ?></strong></td>
                </tr>
            <?php endforeach; ?>
        </table>
            </form>

      
    </body>
</html>
