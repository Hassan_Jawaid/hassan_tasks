<?php

include("connect.php");


$get_salary = ORM:: for_table('employees')
                 ->select_many('employee_id', 'first_name', 'last_name')
                 ->where_raw('salary > (SELECT AVG(salary) FROM employees)')
                 ->find_many();

$count = count($get_salary);

?>

<html>
    <head>
        <title>Practice Query</title>
    </head>

    <body>
    
        <?php echo $count; ?>
        <form method="post" action="">
        <table width='auto' border='1'>
            <tr>
                <th>Employee ID</th> 
                <th>First Name</th>
                <th>Last Name</th>                
            </tr>
            <?php foreach ($get_salary as $user_info): ?>
                <tr>
                   <td><?php echo $user_info->employee_id; ?></strong></td>&nbsp;
                   <td><?php echo $user_info->first_name; ?></strong></td>
                   <td><?php echo $user_info->last_name; ?></strong></td>
                </tr>
            <?php endforeach; ?>
        </table>
            </form>

      
    </body>
</html>
