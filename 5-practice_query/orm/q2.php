<?php

include("connect.php");


$get_department = ORM:: for_table('employees')
                  ->table_alias('e')
                  ->select_many('e.employee_id','e.first_name','e.last_name')
                  ->join('departments',array('e.department_id', '=' , 'departments.department_id'))
                  ->WHERE('departments.department_name' , 'IT')
                  ->find_many();



$count = count($get_department);

?>

<html>
    <head>
        <title>Practice Query</title>
    </head>

    <body>
    
        <?php echo $count; ?>
        <form method="post" action="">
        <table width='auto' border='1'>
            <tr>
                <th>Employee ID</th> 
                <th>First Name</th>
                <th>Last Name</th>                
            </tr>
            <?php foreach ($get_department as $user_info): ?>
                <tr>
                   <td><?php echo $user_info->employee_id; ?></strong></td>&nbsp;
                   <td><?php echo $user_info->first_name; ?></strong></td>
                   <td><?php echo $user_info->last_name; ?></strong></td>
                </tr>
            <?php endforeach; ?>
        </table>
            </form>

      
    </body>
</html>
