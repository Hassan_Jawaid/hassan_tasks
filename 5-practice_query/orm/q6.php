<?php

include("connect.php");


$get_min_salary = ORM::for_table('employees')
                 ->table_alias('emp')
                 ->select_many('emp.employee_id', 'emp.first_name', 'emp.last_name','emp.salary')
                 ->join('jobs', array('job.job_id', '=', 'emp.job_id'),'job')
                 ->where_raw('emp.salary = job.min_salary')
                 ->find_many();

// print_r(ORM::get_last_query());


$count = count($get_min_salary);

?>

<html>
    <head>
        <title>Practice Query</title>
    </head>

    <body>
    
        <?php echo $count; ?>
        <form method="post" action="">
        <table width='auto' border='1'>
            <tr>
                <th>Employee ID</th> 
                <th>First Name</th>
                <th>Last Name</th>
                <th>Salary</th>                
            </tr>
            <?php foreach ($get_min_salary as $user_info): ?>
                <tr>
                   <td><?php echo $user_info->employee_id; ?></strong></td>&nbsp;
                   <td><?php echo $user_info->first_name; ?></strong></td>
                   <td><?php echo $user_info->last_name; ?></strong></td>
                   <td><?php echo $user_info->salary; ?></strong></td>
                </tr>
            <?php endforeach; ?>
        </table>
            </form>

      
    </body>
</html>
