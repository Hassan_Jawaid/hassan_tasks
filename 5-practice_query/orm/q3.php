<?php

include("connect.php");


$get_manager = ORM:: for_table('employees')
               ->table_alias('emp')
               ->select_many('emp.employee_id', 'emp.first_name', 'emp.last_name')
               ->join('employees', array('employees.employee_id' , '=' , 'emp.manager_id'))
               ->join('departments', array('employees.department_id' , '=' , 'departments.department_id'))
               ->join('locations', array('departments.location_id' , '=' , 'locations.location_id'))
               ->WHERE('locations.country_id' , 'US')
               ->find_many();


$count = count($get_manager);

?>

<html>
    <head>
        <title>Practice Query</title>
    </head>

    <body>
    
        <?php echo $count; ?>
        <form method="post" action="">
        <table width='auto' border='1'>
            <tr>
                <th>Employee ID</th> 
                <th>First Name</th>
                <th>Last Name</th>                
            </tr>
            <?php foreach ($get_manager as $user_info): ?>
                <tr>
                   <td><?php echo $user_info->employee_id; ?></strong></td>&nbsp;
                   <td><?php echo $user_info->first_name; ?></strong></td>
                   <td><?php echo $user_info->last_name; ?></strong></td>
                </tr>
            <?php endforeach; ?>
        </table>
            </form>

      
    </body>
</html>
