<?php

include("connect.php");

$get_salary = "SELECT `employee_id`,`first_name`,`last_name` FROM `employees` WHERE salary > (SELECT AVG(salary) FROM employees)";

$query = mysqli_query($conn, $get_salary);
$count = mysqli_num_rows($query);

?>

<html>
    <head>
        <title>Practice Query</title>
    </head>

    <body>
    
        <?php echo $count; ?>
        <form method="post" action="">
        <table width='auto' border='1'>
            <tr>
                <th>Employee ID</th> 
                <th>First Name</th>
                <th>Last Name</th>                
            </tr>
            <?php  while ($result = mysqli_fetch_assoc($query)) {

            $employee_id = $result['employee_id'];

            $first_name = $result['first_name'];

            $last_name = $result['last_name'];

            ?>
                <tr>
                   <td><?php echo $employee_id; ?></strong></td>&nbsp;
                   <td><?php echo $first_name; ?></strong></td>
                   <td><?php echo $last_name; ?></strong></td>
                </tr>
            <?php

      }
          ?>
        </table>
            </form>

      
    </body>
</html>
