<?php

include("connect.php");

$get_min_salary = "SELECT emp.employee_id,emp.first_name,emp.last_name,emp.salary FROM employees AS emp
               INNER JOIN jobs ON jobs.job_id = emp.job_id
               WHERE emp.salary = jobs.min_salary";

$query = mysqli_query($conn, $get_min_salary);
$count = mysqli_num_rows($query);

?>

<html>
    <head>
        <title>Practice Query</title>
    </head>

    <body>
    
        <?php echo $count; ?>
        <form method="post" action="">
        <table width='auto' border='1'>
            <tr>
                <th>Employee ID</th> 
                <th>First Name</th>
                <th>Last Name</th>
                <th>Salary</th>                
            </tr>
            <?php  while ($result = mysqli_fetch_assoc($query)) {

            $employee_id = $result['employee_id'];

            $first_name = $result['first_name'];

            $last_name = $result['last_name'];

            $salary = $result['salary'];

            ?>
                <tr>
                   <td><?php echo $employee_id; ?></strong></td>&nbsp;
                   <td><?php echo $first_name; ?></strong></td>
                   <td><?php echo $last_name; ?></strong></td>
                   <td><?php echo $salary; ?></strong></td>
                </tr>
            <?php

      }
          ?>
        </table>
            </form>

      
    </body>
</html>
