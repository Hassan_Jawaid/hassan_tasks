<?php

include('connect.php');

$user_id = $_GET['id'];

// $update_info = ORM::for_table('user_information')->use_id_column('user_id')->find_one($user_id);
// $update_info->update();

// $update_info->save();

$update_info = ORM::for_table('user_information')->use_id_column('user_id')->find_one($user_id);

if (!empty($_POST)) {        
                      
        $update_info->set($_POST);

		$update_info->save();

        header('Location: index.php ');

        exit;
    }


?>

<html>
    <head>
        <title>Crud Application</title>
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    </head>

    <body>
    
        <h1 class="text-center">Crud Application Demo</h1>
        <h3 class="text-center">Update User Information</h3>

    <div class="container panel panel-default">
            <div class="panel-body">
                <form class="form-horizontal" method="POST">
                <div class="form-group">
                    <label class="col-sm-2 control-label">User ID : </label>
                    <div class="col-sm-10">
                        <input type="text" name="user_name" class="form-control input-lg" value="<?php echo $user_id;?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">First Name : </label>
                    <div class="col-sm-10">
                        <input type="text" name="user_name" class="form-control input-lg" value="<?php echo $update_info->user_name;?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Last Name : </label>
                    <div class="col-sm-10">
                        <input type="text" name="user_last_name" class="form-control input-lg" value="<?php echo $update_info->user_last_name;?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Contact No : </label>
                    <div class="col-sm-10">
                        <input type="number" name="user_contact_no" class="form-control input-lg" value="<?php echo $update_info->user_contact_no;?>">
                    </div>
                </div> 
                <button type="submit" class="btn btn-primary pull-right">Update</button>
            </form>
            </div>
         </div>     
    </body>
</html>