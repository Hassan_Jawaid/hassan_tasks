<?php

include('connect.php');

if (!empty($_POST)) {
        
        $user_info = ORM::for_table('user_information')->create();

        $user_info->set(array('user_name' => $_POST['user_name'], 'user_last_name' => $_POST['user_last_name'] , 'user_contact_no' => $_POST['user_contact_no']));

        $user_info->save();

        header('Location: ' . basename(__FILE__));

        exit;
    }



$count = ORM::for_table('user_information')->count();

?>

<html>
    <head>
        <title>Crud Application</title>
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    </head>

    <body>
    
        <h1 class="text-center">Crud Application Demo</h1>
        <h2 class="text-center">User Info List are : <?php echo $count; ?></h2>
        <h3 class="text-center">Add User Information</h3>
        
        <div class="container panel panel-default">
            <div class="panel-body">
                <form class="form-horizontal" method="POST">
                <div class="form-group">
                    <label class="col-sm-2 control-label">First Name : </label>
                    <div class="col-sm-10">
                        <input type="text" name="user_name" class="form-control input-lg" placeholder="Enter first name..." required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Last Name : </label>
                    <div class="col-sm-10">
                        <input type="text" name="user_last_name" class="form-control input-lg" placeholder="Enter last name..." required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Contact No : </label>
                    <div class="col-sm-10">
                        <input type="number" name="user_contact_no" class="form-control input-lg" placeholder="Enter contact number..." required>
                    </div>
                </div> 
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
            </form>
            </div>
         </div>     

       <div class="container">

        <table id="data_table" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>User Id</th>
                <th>User Name</th>
                <th>User Last Name</th>
                <th>Contact No</th>
                <th>Action</th>
            </tr>
        </thead>
        
    </table>
       </div>
      <script type="text/javascript" src="scripts/jquery-3.2.1.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="scripts/script.js"></script>
    </body>
    
</html>