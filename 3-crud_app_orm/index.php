<?php

include('connect.php');

if (!empty($_POST)) {
        
        $user_info = ORM::for_table('user_information')->create();

        $user_info->set(array('user_name' => $_POST['user_name'], 'user_last_name' => $_POST['user_last_name'] , 'user_contact_no' => $_POST['user_contact_no']));

        $user_info->save();

        header('Location: ' . basename(__FILE__));

        exit;
    }



$count = ORM::for_table('user_information')->count();
$user_list = ORM::for_table('user_information')->find_many();

?>

<html>
    <head>
        <title>Crud Application</title>
    </head>

    <body>
    
        <h1>Crud Application Demo</h1>
        <h2>User Info List are : <?php echo $count; ?></h2>
       
        <table width='auto' border='1'>
            <tr>
                <th>User Id</th>
                <th>User Name</th>
                <th>User Last Name</th>
                <th>Contact No</th>
                <th colspan="2">Action</th>
            </tr>
            <?php foreach ($user_list as $user_info): ?>
                <tr>
                   <?php  $user_id = $user_info->user_id; ?>
            
                   <td><?php echo $user_info->user_id; ?></strong></td>
                   <td><?php echo $user_info->user_name; ?></strong></td>
                   <td><?php echo $user_info->user_last_name; ?></strong></td>
                   <td><?php echo $user_info->user_contact_no; ?></td>
                   <td><a href="update.php?id=<?php echo $user_id;?> ">Update</a> </td>
                   <td><a href="delete.php?id=<?php echo $user_id;?>">Delete</a></td>
                </tr>
            <?php endforeach; ?>
        </table>
       
        <form method="post" action="">
            <h3>Add User Information</h3>
            <label for="name"> User Name :
            <input type="text" name="user_name" /></label>
            <label for="lastname">User Last Name:
            <input type="text" name="user_last_name" /></label>
            <label for="contactno">Contact No:
            <input type="number" name="user_contact_no" /></label>
            <input type="submit" value="Create"  />

        </form>
    </body>
</html>