<?php

    // ------------------- //
    // --- Idiorm Demo --- //
    // ------------------- //

    // Note: This is just about the simplest database-driven webapp it's possible to create
    // and is designed only for the purpose of demonstrating how Idiorm works.

    // Require the idiorm file
    require_once("idiorm.php");

    // Connect to the demo database file
    ORM::configure('mysql:host=localhost;dbname=demo_crud');
    ORM::configure('username', 'root');
    ORM::configure('password', '');

    // Handle POST submission
    if (!empty($_POST)) {
        
        // Create a new user info object
        $user_info = ORM::for_table('user_information')->create();

        // SHOULD BE MORE ERROR CHECKING HERE!

        // Set the properties of the object
        $user_info->user_name = $_POST['user_name'];
        $user_info->user_last_name = $_POST['user_last_name'];
        $user_info->user_contact_no = $_POST['user_contact_no'];

        // Save the object to the database
        $user_info->save();
        header('Location: ' . basename(__FILE__));
        exit;
    }
    if (!empty($_POST)) {

        // Create a new user info object
        $delete_info = ORM::for_table('user_information')->create();

        // SHOULD BE MORE ERROR CHECKING HERE!

        // Set the properties of the object
        $delete_info ->user_id = $_POST['delete'];
        $delete_info = ORM::for_table('user_information')->find_one(user_id);
        $delete_info->delete();

        // Save the object to the database
        $delete_info->save();
        header('Location: ' . basename(__FILE__));
        exit;

        }
        // Get a list of all user_info from the database
        $count = ORM::for_table('user_information')->count();
        $user_list = ORM::for_table('user_information')->find_many();

?>

<html>
    <head>
        <title>Crud Application</title>
    </head>

    <body>
    
        <h1>Crud Application Demo</h1>
        <h2>User Info List are : <?php echo $count; ?></h2>
        <form method="post" action="">
        <table width='auto' border='1'>
            <tr>
                <th>User Id</th>
                <th>User Name</th>
                <th>User Last Name</th>
                <th>Contact No</th>
                <th>Action</th>
            </tr>
            <?php foreach ($user_list as $user_info): ?>
                <tr>
                   <td><?php echo $user_info->user_id; ?></strong></td>&nbsp;
                   <td><?php echo $user_info->user_name; ?></strong></td>
                   <td><?php echo $user_info->user_last_name; ?></strong></td>
                   <td><?php echo $user_info->user_contact_no; ?></td>&nbsp;&nbsp;
                   <td><input type="submit" value="Delete" name="delete"/> </td>
                </tr>
            <?php endforeach; ?>
        </table>
            </form>

        <form method="post" action="">
            <h3>Add User Information</h3>
            <label for="name"> User Name :
            <input type="text" name="user_name" /></label>
            <label for="lastname">User Last Name:
            <input type="text" name="user_last_name" /></label>
            <label for="contactno">Contact No:
            <input type="number" name="user_contact_no" /></label>
            <input type="submit" value="Create"  />

        </form>
    </body>
</html>
