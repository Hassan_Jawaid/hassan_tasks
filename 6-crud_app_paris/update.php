<?php

include('connect.php');
include('insert_model.php');

$user_id = $_GET['id'];


$update_info = Model:: factory('UserInformation')->find_one($user_id);

if (!empty($_POST)) {        
                      
        $update_info->set($_POST);

		$update_info->save();

        header('Location: index.php ');

        exit;
    }


?>

<html>
    <head>
        <title>Crud Application</title>
    </head>

    <body>
    
        <h1>Crud Application Demo</h1>
              
        <form method="post" action="">
            <h3>Update User Information</h3>
            <label for="name"> User ID :
            <input type="text" name="user_name" value="<?php echo $user_id;?>" readonly /></label>
            <label for="name"> User Name :
            <input type="text" name="user_name" value="<?php echo $update_info->user_name;?>" /></label>
            <label for="lastname">User Last Name:
            <input type="text" name="user_last_name" value="<?php echo $update_info->user_last_name;?>"/></label>
            <label for="contactno">Contact No:
            <input type="number" name="user_contact_no" value="<?php echo $update_info->user_contact_no;?>"/></label>
            <input type="submit" value="Update"  />

        </form>
    </body>
</html>