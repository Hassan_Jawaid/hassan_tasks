<?php
error_reporting(0);
include("connect.php");
$sql = "SELECT * FROM list ORDER BY ItemPosition";
$query = mysqli_query($conn, $sql);
$id1 = $_GET['id'];
?>


<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <link rel="stylesheet" href="style.css" type="text/css" />
  <script type="text/javascript" src="Scripts/jquery-3.2.1.js"></script>
  <script type="text/javascript" src="Scripts/script.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


  <title></title>
</head>

<body>
  <div id="page-wrap">
    <div id="header">
      <h1><a href="">PHP Sample Test App</a></h1>
    </div>

    <div id="main">
      <noscript>This site just doesn't work, period, without JavaScript</noscript>

      <ul id="list" class="ui-sortable">
        

       
          <?php  while ($result = mysqli_fetch_assoc($query)) {

          	$id = $result['ListItemId'];

            $done = $result['IsDone'];

            $color = $result['ListColor'];
           
            ?>
          	<li color='1' class='colorBlue ui-state-default <?php if ($done == 1): ?>line<?php endif; ?>' rel='6' data-id='<?php echo $id ?>'>
                   
		          	<span id='<?php echo $id ?>' title='Double-click to edit...' style="background-color: <?php if($color) echo $color;?> " class="click"><?php echo $id.'-'.$result['Description'];?></span>

		            <div class='draggertab tab'></div>

    				    <div class='colortab tab' id='<?php echo $id ?>'><input class="colorpicker" type="color" style="border:none;opacity: 0;"></div>

    				    <div class='deletetab tab'  title='Double-click to delete.' id='<?php echo $id ?>'></div>

    					  <div class='donetab tab' id='<?php echo $id ?>'></div>	

			      </li>
       
            <?php

      }
          ?>

     
      </ul>
	  <br />

      <form action="list.php" id="add-new" method="POST">
      
        <input type="text" id="new-list-item-text" name="new-list-item-text" class="insert" required/>
        <input type="submit" id="add-new-submit" value="Add" class="button"  />
      
      </form>

      <br>


      <div class="clear"></div>
    </div>
  </div>
</body>
</html>

