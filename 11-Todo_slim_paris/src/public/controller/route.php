<?php

$app->get('/todo_slim', function ($request, $response) {
	
	
    $list_details = Model::factory('ListDetails')->order_by_asc('ItemPosition')->find_many();

    return $this->view->render($response, 'list_details.php', array('data' => $list_details)); 

});


$app->post('/insert', function ($request, $response) {

    $list_data = Model::factory('ListDetails')->create();

    date_default_timezone_set("Asia/Karachi");
  	$current_date = date("Y-m-d H:i:s");

  	$max_position = Model::factory('ListDetails')-> max('ItemPosition');
  	$position = (int) $max_position;
  	$new_position = $position+1;

    $list_data->set(array('Description' => $_POST['new-list-item-text'], 'IsDone' => 0 , 'ItemPosition' => $new_position , 'ListColor' => 0 , 'CreateDt' => $current_date));

    $list_data->save();

    return $response->withRedirect('/Todo_slim_paris/src/public/todo_slim');

});


$app->post('/position', function ($request, $response) {
    
	$data = $request->getParsedBody();
	
	$order = $data['order'];

	foreach ($order as $order_value => $order_id) {
		$done_list_item = Model::factory('ListDetails')->find_one($order_id);

		$done_list_item -> set('ItemPosition', $order_value);

		$done_list_item -> save();
	}
	
	

});


$app->post('/color', function ($request, $response) {
    
	$data = $request->getParsedBody();
	
	$id = $data['color_id'];

	$color_value = $data['newcolor'];
	
	$done_list_item = Model::factory('ListDetails')->find_one($id);

	$done_list_item -> set('ListColor', $color_value);

	$done_list_item -> save();

});


$app->post('/update', function ($request, $response) {
    
	$data = $request->getParsedBody();
	
	$id = $data['data_id'];

	$new_data = $data['newvalue'];
	
	$done_list_item = Model::factory('ListDetails')->find_one($id);

	$done_list_item -> set('Description', $new_data);

	$done_list_item -> save();

});


$app->post('/done', function ($request, $response) {
    
	$data = $request->getParsedBody();
	
	$id = $data['done_id'];
	
	$done_list_item = Model::factory('ListDetails')->find_one($id);

	$done_list_item -> set('IsDone', 1);

	$done_list_item -> save();

});


$app->delete('/delete', function ($request, $response) {
    
	$data = $request->getParsedBody();
	
	$id = $data['del_id'];
	
	$delete_list_item = Model::factory('ListDetails')->find_one($id);

	$delete_list_item -> delete();

});

?>