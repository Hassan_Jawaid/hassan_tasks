<html>
    <head>
        <title>Crud Application</title>
        <link rel="stylesheet" type="text/css" href="css/crud.css">
        <script type="text/javascript" src="Scripts/jquery-3.2.1.js"></script>
        <script type="text/javascript" src="Scripts/data.js"></script>
    </head>

    <body>
    
        <h1>Crud Application Demo</h1>
        <h2>User Info List are : <?php echo $count; ?></h2>
    
        <table width='auto' border='1'>
            <tr>
                <th>User Id</th>
                <th>User Name</th>
                <th>User Last Name</th>
                <th>Contact No</th>
                <th colspan="2">Action</th>
            </tr>
            <?php foreach ($data as $user_info): ?>

                <?php  $user_id = $user_info->user_id; ?>

                <tr data-id="<?php echo $user_id;?>" class="row_data">            
                   <td><?php echo $user_info->user_id; ?></strong></td>
                   <td><?php echo $user_info->user_name; ?></strong></td>
                   <td><?php echo $user_info->user_last_name; ?></strong></td>
                   <td><?php echo $user_info->user_contact_no; ?></td>
                   <td><a href="update/<?php echo $user_id ?>">Update</a> </td>
                   <td><p class="delete">Delete</p></td>
                </tr>

            <?php endforeach; ?>
        </table>
       
        <form method="post" action="insert">
            <h3>Add User Information</h3>
            <label for="name"> User Name :
            <input type="text" name="user_name" /></label>
            <label for="lastname">User Last Name:
            <input type="text" name="user_last_name" /></label>
            <label for="contactno">Contact No:
            <input type="number" name="user_contact_no" /></label>
            <input type="submit" value="Create"  />

        </form>
    </body>
</html>