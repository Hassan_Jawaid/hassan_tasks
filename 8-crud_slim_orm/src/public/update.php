
<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require 'idiorm.php';
require 'paris.php';
require 'db.php';

$app = new \Slim\App;
$container = $app->getContainer();

// Register component on container
$container['view'] = function ($container) {
return new \Slim\Views\PhpRenderer('../view');
};

$app->get('/todo',function($request,$response){
$id = $request->getAttribute('id');
$aData = Model::factory('todos')->find_array();
return $this->view->render($response, 'crud.php', array('data' => $aData));
}); 

$app->get('/todo/{id}',function($request,$response){

$id = $request->getAttribute('id');
$todo = ORM::for_table('todolist')->find_one($id);
$aData = array();
if($todo)
{
$aData=$todo->as_array();
}
return $this->view->render($response, 'crud.php', array('data' => $aData));
});

// INSERT // UPDATE
$app->post('/insert',function($request,$response){

$aPost= $request->getParsedBody();
$id=$aPost['id'];
if($id==''){
$model=Model::factory('todos')->create(); 
} 
else {
$model = Model::factory('todos')->find_one($id);
}
unset($aPost['íd']);
$model->set($aPost);
$model->save();
return json_encode($model->as_array());
});

// DELETE 
$app->post('/delete',function($request,$response){
$aPost= $request->getParsedBody();
$id=$aPost['id'];
$delete = Model::factory('todos')->find_one($id);
$delete->delete();
return json_encode($id);
});
$app->run();
?>
Abuzar • Now
