<html>
    <head>
        <title>Crud Application</title>
    </head>

    <body>
    
        <h1>Crud Application Demo</h1>
        
        <form method="post" action="<?php echo SERVER;?>/insert">
            
            <h3>Update User Information</h3>
            <label for="name"> User ID :
            <input type="text" name="user_id" value="<?php echo $data->user_id; ?>" readonly /></label>
            <label for="name"> User Name :
            <input type="text" name="user_name" value="<?php echo $data->user_name; ?>"/></label>
            <label for="lastname">User Last Name:
            <input type="text" name="user_last_name" value="<?php echo $data->user_last_name; ?>"/></label>
            <label for="contactno">Contact No:
            <input type="number" name="user_contact_no" value="<?php echo $data->user_contact_no; ?>"/></label>
            <input type="submit" value="Update"  />

        </form>
    </body>
</html>