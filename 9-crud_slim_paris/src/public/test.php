<?php

$app->get('/crud_slim', function ($request, $response) {
	
	
    $user_details = Model::factory('UserInformation')->find_many();

    $count = count($user_details);

    return $this->view->render($response, 'show.php', array('count' => $count, 'data' => $user_details)); 

    // $response->getBody()->write($server_name);

    // return $response;

});


$app->post('/insert', function ($request, $response) {

	$data = $request->getParsedBody();
	
	if ($data['user_id']) {
	    	$user_information = Model::factory('UserInformation')->find_one($id);
	    }    
	else{
			$user_information = Model::factory('UserInformation')->create();		
	}    

	$user_information -> set($data);

	$user_information->save();

	return $response->withRedirect('/crud_slim_orm/src/public/crud_slim');

});
                   

$app->get('/update/{id}', function ($request, $response) {
    
	$id = $request->getAttribute('id');
	
	$user_details = Model::factory('UserInformation')->find_one($id);

	return $this->view->render($response, 'update.php', array('data' => $user_details)); 

});



$app->delete('/delete', function ($request, $response) {
    
	$data = $request->getParsedBody();
	
	$id = $data['id'];
	
	$delete_info = Model::factory('UserInformation')->find_one($id);

	$delete_info -> delete();

});

?>