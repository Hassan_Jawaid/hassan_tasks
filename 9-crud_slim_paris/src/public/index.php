<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once('../../vendor/autoload.php');
require_once('connect.php');
require_once('model/table.php');

$app = new \Slim\App;

require_once('test.php');

$container = $app->getContainer();


$container['view'] = function ($container) {
    return new \Slim\Views\PhpRenderer('views/');
};


$app->run();

?>